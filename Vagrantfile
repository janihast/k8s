# -*- mode: ruby -*-
# vi: set ft=ruby :

ENV['VAGRANT_DEFAULT_PROVIDER'] = 'libvirt'

Vagrant.configure("2") do |config|

  config.vm.provider :virtualbox do |v|
    v.memory = 2048
    v.cpus = 2
  end

  config.vm.provider :libvirt do |v|
    v.memory = 2048
    v.cpus = 2
  end

  config.vm.provision :shell, privileged: true, inline: $install_common_tools
#  config.vm.synced_folder "/home/jani/k8s", "/vagrant", :nfs =>true, nfs_export: false, nfs_udp: false, nfs_version: 4.0, linux__nfs_options: ["proto=tcp"]

  config.vm.define :proxy do |proxy|
    proxy.vm.box = "peru/ubuntu-18.04-server-amd64"
    proxy.vm.hostname = "proxy"
    proxy.vm.network :private_network, ip: "10.0.0.2"
    proxy.vm.provision :shell, privileged: true, inline: $provision_load_balancer
  end

  config.vm.define :master do |master|
    master.vm.box = "peru/ubuntu-18.04-server-amd64"
    master.vm.hostname = "master"
    master.vm.network :private_network, ip: "10.0.0.10"
    master.vm.provision :shell, privileged: true, inline: $install_k8s_tools
    master.vm.provision :shell, privileged: false, inline: $provision_master_node
  end

  %w{worker1 worker2}.each_with_index do |name, i|
    config.vm.define name do |worker|
      worker.vm.box = "peru/ubuntu-18.04-server-amd64"
      worker.vm.hostname = name
      worker.vm.network :private_network, ip: "10.0.0.#{i + 11}"
      worker.vm.provision :shell, privileged: true, inline: $install_k8s_tools
      worker.vm.provision :shell, privileged: false, inline: <<-SHELL
while [ ! -f /vagrant/master ]; do echo "sleeping"; sleep 1; done
sudo /vagrant/join.sh
echo 'Environment="KUBELET_EXTRA_ARGS=--node-ip=10.0.0.#{i + 11}"' | sudo tee -a /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
sudo systemctl daemon-reload
sudo systemctl restart kubelet
touch /vagrant/worker#{i}
SHELL
    end
  end

  config.vm.provision "shell", inline: $install_multicast
end

$provision_load_balancer = <<-PROXY
export DEBIAN_FRONTEND=noninteractive
apt-get -qq update
apt-get -qq install -y nginx

cat <<EOF >/etc/ssl/certs/nginx-selfsigned.crt
-----BEGIN CERTIFICATE-----
MIID0TCCArmgAwIBAgIUN1KguZLcB8oyueiFGBKNzC5IOV0wDQYJKoZIhvcNAQEL
BQAweDELMAkGA1UEBhMCRkkxEzARBgNVBAgMClNvbWUtU3RhdGUxDTALBgNVBAcM
BENpdHkxEDAOBgNVBAoMB0NvbXBhbnkxFDASBgNVBAMMCyouazhzLmludHJhMR0w
GwYJKoZIhvcNAQkBFg5yb290QGs4cy5pbnRyYTAeFw0yMDA4MTIxMzQ0NTdaFw0y
MTA4MTIxMzQ0NTdaMHgxCzAJBgNVBAYTAkZJMRMwEQYDVQQIDApTb21lLVN0YXRl
MQ0wCwYDVQQHDARDaXR5MRAwDgYDVQQKDAdDb21wYW55MRQwEgYDVQQDDAsqLms4
cy5pbnRyYTEdMBsGCSqGSIb3DQEJARYOcm9vdEBrOHMuaW50cmEwggEiMA0GCSqG
SIb3DQEBAQUAA4IBDwAwggEKAoIBAQDFJ2oL17nzX8kOX/n86+ezN9v+ARk63/q7
K9z5toy+o2kPigDQtA37YiokEu134vNkVvxQnBa16PY0P6HARm3ATtVEZAlJuAN5
3n3I3QWjXJB02lvcN1obake5QfxDkBf0eqF7go3wWuvbp9mkdXoTdB5zl0d1ajHJ
QAjtrvaemhkDSgyglMYxumCuou/RDeSsbnZMwVONcYdVBj1O9tdArxnkU2+bn5Xq
E13sZq8hXEgvXyczau6scjQfomg4ceGGjccAhwme6CrvmxlL2LkytWdqfzjQpjFs
yhmnmJyZ3UNcZZBHlvLblv5hFcgvZsi3xsY3ReE7uMyooLO22Me9AgMBAAGjUzBR
MB0GA1UdDgQWBBQ+fGndf0Nm+cQMcIBnLnBesg8zqjAfBgNVHSMEGDAWgBQ+fGnd
f0Nm+cQMcIBnLnBesg8zqjAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUA
A4IBAQBGiXbwOcqghuR7AdfBPBNh71NH96lcPjAjjMC0yJfa0xY2VBrOQZHsFh53
2Ni0Tc4XibfhpRkFWp207z+U3hDI8PzGyqBSrpoZI4d33df1SHdBkcO4NhlSiCWN
kNVI4Y9ol3c4Ll2rm7Gtc9llqIArl6vmeN1pF8GgGhSRypqwoLup8WdnUj4iHh1M
aU33YgSXi9jOfQ08RRBqd49M7dDWLkhxHKrGw+1pN6gaijc6ly0HVX6yDMB5tYMk
vJH6RAr7pCD3pLVJdz8yxRH4jO77vvTDihAMx/L9fLqWzIqy/WEOui8TUw5TFnkY
nDYIGSUSTVDj4NEm6I/yLtH158yR
-----END CERTIFICATE-----
EOF

cat <<EOF >/etc/ssl/private/nginx-selfsigned.key
-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQC6Jueu/ndfA6c1
3ufS7GAbYW3ONBqukVuH7njU08mIZYaAfZZ3QQmHzk55QhnKXHJ/OzyvoXiEyD8m
ZQc4ZRjFhRkvTwWnrhU2IasbFnRJrTmdbV6L+rppHoq8+eWmLnm5B/Sv0faaxd42
ku9wKCGVujsYqIFSVzC/oCp5v+iE0ObIEkH59M81Qu5ocaOOnc/qVam5zoYsgr/o
3mxssZQwTgAWYKx+MmAZ0vKVeVH/KWxvCjTrkDXHYIGZhY0obQD1Upha3ZpaGQ5X
mSOZ2rw3Dv2+fOa50X0uX5YPPp5PrgLt6BATBQ6O5ytRFFwG0i7G/VF+KRUZUbLB
JYCJT8MDAgMBAAECggEAfIbEG/NMZXorJEVuLVJE5GwjqXp/kuHYKjGkETNwMNdB
x7i7rCEMwBWJt2I5oH6BdY1qBk9tMN5IKGYTE23FyXRFKbmiHc69j82sCd3Wgubn
kOF2Cwv6IZH1238MWkPi151z2R4K6vSMt1NJ62xdMy02lUM755o+GSKK55EyeY2k
78sFFKl6ilcGAK18ukpG6yNnoHZvaX4pKX+cr9M5Jox0nOmOe+u7c1Ke5CBjKAy1
Iv2unbE6/eGJ8jlwSsTsa1VNN8FsCez3RYlJb2JNLmltB29728KwL2SoLMwCGc1O
MyqpQtCq+VhGFAxRuESdGFNfcfEgKNoZ4im22ugsiQKBgQDpNszoCOOl7p4ppF9w
1wvFkeJsBKggk6GIrskpHIgNrlny/hsgRVqs6Rikz75DVFhk6OFQvAIjDdSAlnt2
e168OryEZIpXLMGM18OqSZ55q9O43+m4ZcP3fGMfRLODAnFXeGZcyW65N07lE3nk
Y1oGhDHHnSXEHyqx3e6VfoIePwKBgQDMVvpDxZKibLNmXSyZ2g6nw6nQ/J3aD+YE
g8Oy4brvdVk+twL3RtOOyDdB4pS6AbwrnlFiG5LncYEq0RIG6KH3WF2D9ZX0PVpl
KIuGfp5MVojG7zbBTTk3yX1/R2Lc53wwgRqxYY1Ulr1IAhBRLXeZtPTG5crAJsHS
aUHyzInyPQKBgEHEtJCTQS/gVTZFJq052BYFiCcSQR++woU7VXuEklHZY4CFbLzz
EhlFOy/vbiaquKrsie4styWPlugUiAjnzdWjctqbR/BBrOu9RW7WiByj+ndenhjk
ihlXONTumPlPTQtv5t4v6EvPby5nCa8A3biMu0Uh6i6e43l5rvj1zlgdAoGANohX
zdJiabp8virLhlutYZRUM5PXRKTNjBLmHU0kqFc29Ae/YOKj/wzrwkeHzYQ9BZ8A
tSAKun2OLTESkIlxACOzAU5/MGlXQHvrkH8mG9VUN1ML19aWGI9LULZqae3VNNSf
nQsdvp9j8F2soR1zN7AVSyd0ToeJXm/8y6FOhzkCgYBWTbVv7O5lmQbs9DMW60kJ
QmmAhMAw8ltepnbAc69JOghQc+ioYUiYOjUg95M3SSS6Dz4lPZ0uwMJWZcTxJEbe
09K1zmrBRmsbWibhqqJIDgtsRebu6kDc+7A9jBFlxenPpuFs/VZE5uOcliv+Mzgd
kXWbQT83f9LDKvBf0uIwlw==
-----END PRIVATE KEY-----
EOF

cat <<EOF >/etc/nginx/sites-available/default
upstream backend_http {
  server 10.0.0.11:30000;
  server 10.0.0.12:30000;
}

upstream backend_https {
  server 10.0.0.11:30001;
  server 10.0.0.12:30001;
}

server {
  client_max_body_size 2048M;

  listen 80 default_server;
  listen [::]:80 default_server;
#    return 302 https://$host$request_uri;

  location / {
    proxy_set_header Host \\$http_host;
    proxy_set_header X-Real-IP \\$remote_addr;
    proxy_set_header X-Forwarded-Proto \\$scheme;
    proxy_set_header X-Forwarded-For \\$proxy_add_x_forwarded_for;
    proxy_pass http://backend_http;
    #proxy_read_timeout 900;
    #proxy_buffering on;
  }
}

#server {
#        listen 443 ssl http2 default_server;
#        listen [::]:443 ssl http2 default_server;
#
#        ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;
#        ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;
#
#        location / {
#            proxy_set_header Host \\$http_host;
#            proxy_set_header X-Real-IP \\$remote_addr;
#            proxy_set_header X-Forwarded-Proto \\$scheme;
#            proxy_set_header X-Forwarded-For \\$proxy_add_x_forwarded_for;
#            proxy_pass http://backend_https;
#            #proxy_read_timeout 900;
#            #proxy_buffering on;
#            proxy_ssl_verify off;
#        }
#}
EOF

systemctl restart nginx
PROXY

$install_common_tools = <<-SCRIPT
test -f /vagrant/worker0 && rm /vagrant/worker0
test -f /vagrant/worker1 && rm /vagrant/worker1
test -f /vagrant/worker2 && rm /vagrant/worker2
test -f /vagrant/master && rm /vagrant/master

# bridged traffic to iptables is enabled for kube-router.
cat >> /etc/ufw/sysctl.conf <<EOF
net/bridge/bridge-nf-call-ip6tables = 1
net/bridge/bridge-nf-call-iptables = 1
net/bridge/bridge-nf-call-arptables = 1
EOF

# disable swap
swapoff -a
sed -i '/swap/d' /etc/fstab

# Install kubeadm, kubectl and kubelet
export DEBIAN_FRONTEND=noninteractive
apt-get -qq update
apt-get -qq install -y curl gnupg nfs-common vim

cat >> /etc/fstab <<EOF
10.0.0.1:/srv/k8s/shared   /vagrant     nfs     rw,_netdev,nfsvers=4                      0 0
EOF

cat >> /etc/hosts <<EOF
10.0.0.2        basic.k8s.intra registry.k8s.intra
EOF

mkdir /vagrant
mount /vagrant
chown vagrant:vagrant /vagrant
SCRIPT

$install_k8s_tools = <<-K8STOOLS
mkdir /etc/docker
cat > /etc/docker/daemon.json <<EOF
{ "insecure-registries" : ["registry.k8s.intra"] }
EOF

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get -qq update
apt-get -qq install -y docker.io apt-transport-https kubelet kubeadm kubectl
systemctl enable docker
K8STOOLS


$provision_master_node = <<-SHELL
test -f /vagrant/master && rm /vagrant/master
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get -qq update
sudo apt-get -qq install -y helm

OUTPUT_FILE=/vagrant/join.sh
test -f $OUTPUT_FILE && sudo rm -rf $OUTPUT_FILE

# Start cluster
sudo kubeadm init --apiserver-advertise-address=10.0.0.10 --pod-network-cidr=10.244.0.0/16 | grep -Ei "kubeadm join|discovery-token-ca-cert-hash" > ${OUTPUT_FILE}

chmod +x $OUTPUT_FILE

# Configure kubectl
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

# Fix kubelet IP
echo 'Environment="KUBELET_EXTRA_ARGS=--node-ip=10.0.0.10"' | sudo tee -a /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

# Configure flannel
#curl -o kube-flannel.yml https://raw.githubusercontent.com/coreos/flannel/v0.9.1/Documentation/kube-flannel.yml
curl -o kube-flannel.yml "https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml"
sed -i.bak 's|"/opt/bin/flanneld",|"/opt/bin/flanneld", "--iface=enp0s8",|' kube-flannel.yml
kubectl create -f kube-flannel.yml

sudo systemctl daemon-reload
sudo systemctl restart kubelet

touch /vagrant/master

while [ ! -f /vagrant/worker0 ]; do echo "sleeping"; sleep 1; done
while [ ! -f /vagrant/worker1 ]; do echo "sleeping"; sleep 1; done

cat <<EOF >/tmp/values.yaml
controller:
  kind: DaemonSet
  daemonset:
    useHostPort: true
    hostPorts:
      http: 80
      https: 443
  service:
    type: NodePort
    nodePorts:
      http: 30000
      https: 30001
    externalIPs:
    - 10.0.0.2
EOF
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm upgrade --install nginx-ingress ingress-nginx/ingress-nginx -f /tmp/values.yaml

sudo cp /etc/kubernetes/admin.conf /vagrant/admin.conf
sudo chown vagrant: /vagrant/admin.conf
SHELL

$install_multicast = <<-SHELL
apt-get -qq install -y avahi-daemon libnss-mdns
SHELL
