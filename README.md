# Setup
```
# /etc/exports: NFS file systems being exported.  See exports(5).
"/home/jani/k8s" 192.168.0.0/16(rw,async,no_root_squash,fsid=root,no_subtree_check,no_wdelay,insecure)

vagrant up
source env
```

